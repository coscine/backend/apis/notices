using Coscine.Api.Notices.ReturnObjects;
using Coscine.Api.Notices.Services;
using Coscine.Api.Notices.Utils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Coscine.Api.Notices.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with notices.
    /// </summary>
    public class NoticeController : Controller
    {
        private readonly INoticeService _noticeService;
        private readonly MaintenanceHelper _maintenanceHelper;
        // url to rss feed from consul variable
        private readonly string _rssUrl;
        private readonly string _deploymentBanner = Program.Configuration.GetStringAndWait($"coscine/global/deploymentBanner");

        /// <summary>
        /// NoticeController constructor
        /// </summary>
        /// <param name="noticeService">Given NoticeService implementation</param>
        public NoticeController(INoticeService noticeService)
        {
            _noticeService = noticeService;
            var undefinedString = "-NotDefined-";

            _maintenanceHelper = new MaintenanceHelper
            {
                RelevanceList = new List<string> { "Eingriff", "Störung", "Teilstörung", "Unterbrechung", "eingeschränkt betriebsfähig", "Wartung", "Teilwartung", "Änderung", "Warnung", undefinedString, "Hinweis" },
                UndefinedString = undefinedString,
            };
            _rssUrl = Program.Configuration.GetStringAndWait($"coscine/global/rss/url");
        }

        /// <summary>
        /// Returns a notice
        /// </summary>
        /// <param name="documentSlug">Slug defining a configured notice</param>
        /// <param name="language">Language (e.g. "en" or "de")</param>
        /// <returns>Notice</returns>
        [Obsolete]
        [HttpGet("[controller]/{documentSlug}")]
        [ResponseCache(Duration = 60 * 60, VaryByQueryKeys = new[] { "documentSlug", "language" })]
        public async Task<IActionResult> GetNotice(string documentSlug, [FromQuery] string language = "en")
        {
            var url = await Program.Configuration.GetStringAsync($"coscine/local/documents/{documentSlug}/{language}", null);

            if (url == null)
            {
                return BadRequest();
            }

            return Json(new 
            {
                data = new
                { 
                    body = await _noticeService.GetNotice(url) 
                }
            });
        }

        /// <summary>
        /// Returns defined properties of the first entry of the rss feed
        /// </summary>
        /// <returns>Maintenance or 404 if no maintenance was found</returns>
        [HttpGet("[controller]/getMaintenance")]
        public ActionResult<MaintenanceReturnObject> GetMaintenance()
        {
            if (_deploymentBanner == "true")
            {
                var deploymentMaintenance = new Maintenance(){
                    DisplayName = "Deployment Coscine",
                    Url = null,
                    Type = "Eingriff",
                    Body = "Aufgrund von Wartungsarbeiten kann es zu Einschränkungen bei der Nutzung von Coscine kommen.",
                    StartsDate = DateTime.Today,
                    EndsDate = DateTime.Today.AddDays(1)
                };

                var deploymentMaintenanceReturnObject = new MaintenanceReturnObject
                {
                    Body = deploymentMaintenance.Body,
                    DisplayName = deploymentMaintenance.DisplayName,
                    EndsDate = deploymentMaintenance.EndsDate,
                    StartsDate = deploymentMaintenance.StartsDate,
                    Type = deploymentMaintenance.Type,
                    Url = deploymentMaintenance.Url
                };

                return Ok(deploymentMaintenanceReturnObject);
            }

            var maintenance = _maintenanceHelper.GetMaintenance(_rssUrl);

            if (maintenance == null)
            {
                return Ok(new MaintenanceReturnObject());
            }

            var maintenanceReturnObject = new MaintenanceReturnObject
            {
                Body = maintenance.Body,
                DisplayName = maintenance.DisplayName,
                EndsDate = maintenance.EndsDate != DateTime.MaxValue ? maintenance.EndsDate : null,
                StartsDate = maintenance.StartsDate != DateTime.MinValue ? maintenance.StartsDate : null,
                Type = maintenance.Type,
                Url = maintenance.Url,
            };

            return Ok(maintenanceReturnObject);
        }

    }
}
