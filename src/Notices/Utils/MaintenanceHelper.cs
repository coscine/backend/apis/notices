﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Xml;

namespace Coscine.Api.Notices.Utils
{
    /// <summary>
    /// This class helps to retrieve the relevant maintenance message for a certain service.
    /// </summary>
    public class MaintenanceHelper
    {
        /// <summary>
        /// How the undefined string should look.
        /// </summary>
        public string UndefinedString { get; set; }

        /// <summary>
        /// Order of relevance to be used.
        /// Only the most relevant (first in the list) maintenace message is later displayed.
        /// All non listed "Störungen" are sorted as "-NotDefined-".
        /// Use this list to adjust the order.
        /// Types which are not part of the list, will be set to the UndefinedString property.
        /// </summary>
        public List<string> RelevanceList { get; set; }

        /// <summary>
        /// Get the most relevant maintenance from the rss feed
        /// </summary>
        /// <param name="feedUrl">Url of the rss feed</param>
        /// <returns>The most relevant Maintenance</returns>
        internal Maintenance GetMaintenance(string feedUrl)
        {
            return GetMaintenance(feedUrl, RelevanceList);
        }

        /// <summary>
        /// Get the most relevant maintenance from the rss feed
        /// </summary>
        /// <param name="feedUrl">URL of the rss feed</param>
        /// <param name="order">Order of Importance</param>
        /// <returns>The most relevant Maintenance</returns>
        internal Maintenance GetMaintenance(string feedUrl, List<string> order)
        {
            Maintenance result = null;

            if (order.FindIndex(x => x.ToLower() == UndefinedString.ToLower()) == -1)
            {
                order.Add(UndefinedString);
            }

            int resultOrderPos = order.Count + 1;
            var rssfeed = SyndicationFeed.Load(XmlReader.Create(feedUrl));
            var syndicationItems = rssfeed.Items.OrderByDescending(x => x.PublishDate).ToList();

            foreach (var syndicationItem in syndicationItems)
            {
                try
                {
                    if (syndicationItem != null && syndicationItem.Summary != null && !string.IsNullOrWhiteSpace(syndicationItem.Summary.Text))
                    {
                        var current = new Maintenance
                        {
                            DisplayName = syndicationItem.Title.Text,
                            Url = syndicationItem.Links[0].Uri
                        };

                        string summary = syndicationItem.Summary.Text;
                        int splitter = summary.IndexOf('-');
                        if (splitter != -1)
                        {
                            current.Body = summary.Substring(splitter + 1, summary.Length - splitter - 1).Trim();
                            string firstPart = summary.Substring(0, splitter).Trim();
                            splitter = firstPart.IndexOf("von");
                            if (splitter != -1)
                            {
                                current.Type = firstPart.Substring(0, splitter).Trim();
                                string timespan = firstPart.Substring(splitter + 3, firstPart.Length - splitter - 3).Trim();

                                splitter = timespan.IndexOf("bis");
                                if (splitter != -1)
                                {
                                    string startsString = timespan.Substring(0, splitter).Trim();
                                    string endsString = timespan.Substring(splitter + 3, timespan.Length - splitter - 3).Trim();

                                    var dayList = new List<string> { "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag" };

                                    foreach (var dayString in dayList)
                                    {
                                        startsString = startsString.Replace(dayString, "").Trim();
                                        startsString = startsString.Replace(dayString.ToLower(), "").Trim();
                                    }

                                    foreach (var dayString in dayList)
                                    {
                                        endsString = endsString.Replace(dayString, "").Trim();
                                        endsString = endsString.Replace(dayString.ToLower(), "").Trim();
                                    }

                                    if (startsString.ToLower() == "unbekannt")
                                    {
                                        current.StartsDate = DateTime.MinValue;
                                    }
                                    else
                                    {
                                        current.StartsDate = DateTime.ParseExact(startsString, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture);
                                    }

                                    if (endsString.ToLower() == "unbekannt")
                                    {
                                        current.EndsDate = DateTime.MaxValue;
                                    }
                                    else
                                    {
                                        current.EndsDate = DateTime.ParseExact(endsString, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture);
                                    }

                                    // still active message
                                    if (current.EndsDate > DateTime.Now && current.StartsDate < DateTime.Now)
                                    {
                                        // first listed valid message
                                        if (result == null)
                                        {
                                            result = current;
                                            resultOrderPos = GetOrderPos(order, result.Type);
                                        }
                                        else
                                        {
                                            // check if the current message is higher ranged as the previosly result
                                            int currentOrderPos = GetOrderPos(order, current.Type);
                                            // higher rang of type
                                            if (currentOrderPos < resultOrderPos)
                                            {
                                                result = current;
                                                resultOrderPos = currentOrderPos;
                                                // same type but longer active
                                            }
                                            else if (currentOrderPos < resultOrderPos && current.EndsDate > result.EndsDate)
                                            {
                                                result = current;
                                                resultOrderPos = currentOrderPos;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    // ignore not correct parsed messages
                }
            }
            return result;
        }

        private int GetOrderPos(List<string> order, string type)
        {
            int res = order.FindIndex(x => x.ToLower() == type.ToLower());

            if (res == -1)
            {
                res = order.FindIndex(x => x.ToLower() == UndefinedString.ToLower());
            }

            if (res == -1)
            {
                res = order.Count + 1;
            }

            return res;
        }
    }
}
