﻿using System;

namespace Coscine.Api.Notices.Utils
{
    /// <summary>
    /// This class represents a maintenance with its significant properties.
    /// </summary>
    public class Maintenance
    {
        /// <summary>
        /// Maintenance title.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Maintenance url.
        /// </summary>
        public Uri Url { get; set; }

        /// <summary>
        /// Maintenance type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Maintenance description.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Maintenance start.
        /// </summary>
        public DateTime StartsDate { get; set; }

        /// <summary>
        /// Maintenance end.
        /// </summary>
        public DateTime EndsDate { get; set; }

    }
}
