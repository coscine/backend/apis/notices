﻿using System.Threading.Tasks;

namespace Coscine.Api.Notices.Services
{
    /// <summary>
    /// This interface defines the functions for the NoticeService
    /// </summary>
    public interface INoticeService
    {
        /// <summary>
        /// This method returns a notice
        /// </summary>
        /// <param name="url">Url to retrieve the notice from</param>
        /// <returns>Notice</returns>
        Task<string> GetNotice(string url);
    }
}
