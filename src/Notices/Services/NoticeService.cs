﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Coscine.Api.Notices.Services
{
    /// <summary>
    /// This class implements INoticeService
    /// </summary>
    public class NoticeService : INoticeService
    {
        private readonly HttpClient _httpClient;

        /// <summary>
        /// This constructor creates a NoticeService object
        /// </summary>
        /// <param name="httpClient">Given HttpClient</param>
        public NoticeService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <summary>
        /// This method returns a notice
        /// </summary>
        /// <param name="url">Url to retrieve the notice from</param>
        /// <returns>Notice</returns>
        public async Task<string> GetNotice(string url)
        {
            return await _httpClient.GetStringAsync(url);
        }
    }
}
