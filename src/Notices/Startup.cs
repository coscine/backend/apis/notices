﻿using Coscine.Api.Notices.Services;
using Coscine.ApiCommons;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Coscine.Api.Notices
{
    /// <summary>
    /// Standard Startup class.
    /// </summary>
    public class Startup : AbstractStartup
    {
        /// <summary>
        /// Standard Startup constructor.
        /// </summary>
        public Startup()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public override void ConfigureExtension(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.ConfigureExtension(app, env);

            app.UseResponseCaching();
        }

        /// <summary>
        /// This method extends the Service configuration
        /// </summary>
        /// <param name="services">ServiceCollection</param>
        public override void ConfigureServicesExtension(IServiceCollection services)
        {
            base.ConfigureServicesExtension(services);

            services.AddResponseCaching();
            services.AddHttpClient<INoticeService, NoticeService>(client =>
            {
                //TODO: Discuss Timeout value
                client.Timeout = TimeSpan.FromMinutes(30);
            });
        }

    }
}
